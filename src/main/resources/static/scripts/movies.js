const movieForm = [
    document.getElementById("movie-name"),
    document.getElementById("movie-director"),
    document.getElementById("movie-category"),
    document.getElementById("movie-age-category"),
    document.getElementById("movie-length"),
    document.getElementById("movie-premiere"),
    document.getElementById("movie-picture"),
    document.getElementById("movie-trailer"),
    document.getElementById("movie-desc")]

function viewMode(id) {
    getMovieData(id);
    document.getElementById("save-button-from").style.display = 'none';
    movieForm.forEach(value => value.disabled = true);
}

function editmode(id, uuid) {
    getMovieData(id);
    document.getElementById('save-button-from').setAttribute('data-uuid', uuid)
    document.getElementById("save-button-from").style.display = 'block';
    movieForm.forEach(value => value.disabled = false);
}


function getMovieData(id) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://' + window.location.host + '/api/v1/movie' + "?id=" + id, true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                document.getElementById("movie-name").value = response.title
                document.getElementById("movie-director").value = response.director
                document.getElementById("movie-category").value = response.movieCategory
                document.getElementById("movie-age-category").value = response.ageCategory
                document.getElementById("movie-length").value = response.movieLength
                document.getElementById("movie-premiere").value = response.premiere
                document.getElementById("movie-picture").value = response.picture
                document.getElementById("movie-trailer").value = response.trailer
                document.getElementById("movie-desc").textContent = response.description
            }
        }
    }
    xhr.send();
}

function editMovie(button) {
    if (!button.getAttribute('data-uuid')) {
        saveMovie();
    } else {
        const movie = {
            uuid: button.getAttribute('data-uuid'),
            title: document.getElementById("movie-name").value,
            director: document.getElementById("movie-director").value,
            movieCategory: document.getElementById("movie-category").value,
            ageCategory: document.getElementById("movie-age-category").value,
            movieLength: document.getElementById("movie-length").value,
            premiere: document.getElementById("movie-premiere").value,
            picture: document.getElementById("movie-picture").value,
            trailer: document.getElementById("movie-trailer").value,
            description: document.getElementById("movie-desc").value
        }
        const jsonData = JSON.stringify(movie);
        const xhr = new XMLHttpRequest();
        xhr.open('PUT', 'http://' + window.location.host + '/api/v1/movie', true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    //TODO succes handler
                    location.reload()
                } else {
                    //TODO error handler
                }
            }
        }

        xhr.send(jsonData);
    }
}

function saveMovie() {
    const movie = {
        title: document.getElementById("movie-name").value,
        director: document.getElementById("movie-director").value,
        movieCategory: document.getElementById("movie-category").value,
        ageCategory: document.getElementById("movie-age-category").value,
        movieLength: document.getElementById("movie-length").value,
        premiere: document.getElementById("movie-premiere").value,
        picture: document.getElementById("movie-picture").value,
        trailer: document.getElementById("movie-trailer").value,
        description: document.getElementById("movie-desc").value
    }
    const jsonData = JSON.stringify(movie);
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://' + window.location.host + '/api/v1/movie', true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                //TODO succes handler
                location.reload()
            } else {
                //TODO error handler
            }
        }
    }

    xhr.send(jsonData);

}


function removeMovie(id, name) {
    document.getElementById('message').textContent = 'Czy napewno chcesz usunąć film o nazwie ' + name;
    document.getElementById('delete-button').setAttribute('data-id', id);
}

function deleteMovie(button) {
    let id = button.getAttribute('data-id')
    const xhr = new XMLHttpRequest();
    xhr.open('DELETE', 'http://' + window.location.host + '/api/v1/movie' + "?id=" + id, true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                //TODO succes handler
                location.reload()
            } else {
                //TODO error handler
            }
        }
    }
    xhr.send();
}