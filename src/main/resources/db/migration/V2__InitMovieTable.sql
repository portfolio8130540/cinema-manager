create table movie
(
    id             serial primary key,
    uuid           varchar,
    title          varchar,
    director       varchar,
    movie_category varchar,
    age_category   varchar,
    movie_length   int,
    premiere       varchar,
    picture        varchar,
    trailer        varchar,
    description    text,
    deleted        boolean default false
);


insert into movie (title,uuid, director, movie_category, age_category, movie_length, premiere, picture, trailer, description,
                   deleted)
values ('Transformers','S1', 'Michael Bay', 'Si-Fi', '12+', 144, '17.08.2007',
        'https://t1.gstatic.com/licensed-image?q=tbn:ANd9GcRL1XgVEINwK9ZkYMu2_3Sw_4RRp7p0WRSVM6mHwvbOR-e_Lc-qMM-o3XPGXRy3GDx4',
        'https://www.youtube.com/watch?v=ahYjx0rQvqQ',
        ' amerykański fantastycznonaukowy film akcji z 2007 roku w reżyserii Michaela Baya. W rolach głównych wystąpili Shia LaBeouf, Jon Voight i Megan Fox. Film powstał na podstawie zabawek z serii Transformers produkowanych przez Metro-Goldwyn-Mayer oraz Hasbro.',
        false);
insert into movie (title,uuid, director, movie_category, age_category, movie_length, premiere, picture, trailer, description,
                   deleted)
values ('Transformers','S2','Michael Bay', 'Si-Fi', '12+', 144, '17.08.2007',
        'https://t1.gstatic.com/licensed-image?q=tbn:ANd9GcRL1XgVEINwK9ZkYMu2_3Sw_4RRp7p0WRSVM6mHwvbOR-e_Lc-qMM-o3XPGXRy3GDx4',
        'https://www.youtube.com/watch?v=ahYjx0rQvqQ',
        ' amerykański fantastycznonaukowy film akcji z 2007 roku w reżyserii Michaela Baya. W rolach głównych wystąpili Shia LaBeouf, Jon Voight i Megan Fox. Film powstał na podstawie zabawek z serii Transformers produkowanych przez Metro-Goldwyn-Mayer oraz Hasbro.',
        false);
insert into movie (title,uuid,director, movie_category, age_category, movie_length, premiere, picture, trailer, description,
                   deleted)
values ('Transformers','S3','Michael Bay', 'Si-Fi', '12+', 144, '17.08.2007',
        'https://t1.gstatic.com/licensed-image?q=tbn:ANd9GcRL1XgVEINwK9ZkYMu2_3Sw_4RRp7p0WRSVM6mHwvbOR-e_Lc-qMM-o3XPGXRy3GDx4',
        'https://www.youtube.com/watch?v=ahYjx0rQvqQ',
        ' amerykański fantastycznonaukowy film akcji z 2007 roku w reżyserii Michaela Baya. W rolach głównych wystąpili Shia LaBeouf, Jon Voight i Megan Fox. Film powstał na podstawie zabawek z serii Transformers produkowanych przez Metro-Goldwyn-Mayer oraz Hasbro.',
        false)