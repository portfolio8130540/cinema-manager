create table user_table
(
    id       serial primary key,
    name     varchar not null,
    email    varchar not null,
    password varchar not null,
    enabled  boolean DEFAULT false,
    role     varchar not null
);

insert into user_table(name, email, password, enabled, role) values ('takus','takus@gmail.com','$2a$12$vhmCX68Gh57tuSbN51e6yufY9Ru8BoB.u92I8TFNKcNApR2ioyN36',true,'ROLE_LOGISTICIAN');
insert into user_table(name, email, password, enabled, role) values ('Ajwuuu','ajwuuu@gmail.com','$2a$12$vhmCX68Gh57tuSbN51e6yufY9Ru8BoB.u92I8TFNKcNApR2ioyN36',true,'ROLE_CHECKER');