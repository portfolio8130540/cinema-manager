package com.example.cinemamanager.exceptions;

public class MovieDontExistException extends RuntimeException{
    public MovieDontExistException() {
        super();
    }

    public MovieDontExistException(String message) {
        super(message);
    }

    public MovieDontExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public MovieDontExistException(Throwable cause) {
        super(cause);
    }

    protected MovieDontExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
