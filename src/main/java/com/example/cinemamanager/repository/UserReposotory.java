package com.example.cinemamanager.repository;

import com.example.cinemamanager.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserReposotory extends JpaRepository<User,Long> {

    Optional<User> findUserByName(String name);
}
