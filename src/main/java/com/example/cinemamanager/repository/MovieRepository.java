package com.example.cinemamanager.repository;

import com.example.cinemamanager.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie,Long> {
    List<Movie> findMoviesByDeletedIsAndUuidOrAgeCategoryOrMovieCategoryOrTitleOrderByUuid(boolean deleted,String uuid,String ageCategory,String movieCategory,String title);
    List<Movie> findMoviesByDeletedIsOrderByUuid(boolean deleted);
    Optional<Movie> findMoviesByDeletedIsAndUuid(boolean deleted,String uuid);
    List<Movie> findMoviesByUuidOrAgeCategoryOrMovieCategoryOrTitleOrderByUuid(String uuid,String ageCategory,String movieCategory,String title);
    Optional<Movie> findMoviesByUuid(String uuid);
}
