package com.example.cinemamanager.services;

import com.example.cinemamanager.entity.User;
import com.example.cinemamanager.entity.UserEntityUserDetails;
import com.example.cinemamanager.repository.UserReposotory;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserEntityUserDetailsService implements UserDetailsService {
    private final UserReposotory userReposotory;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userEntity = userReposotory.findUserByName(username);
        return userEntity.map(UserEntityUserDetails::new).orElseThrow(()-> new UsernameNotFoundException("User not found "+username));
    }
}