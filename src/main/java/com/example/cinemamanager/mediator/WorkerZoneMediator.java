package com.example.cinemamanager.mediator;

import com.example.cinemamanager.entity.Movie;
import com.example.cinemamanager.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class WorkerZoneMediator {
    private final MovieService movieService;


    public List<Movie> findAllByDisabledAndSearch(Integer disabled,String search){
        boolean isDisabled = false;
        switch (disabled){
            default:
                return movieService.findAllByDisabled(false,search);
            case 1:
                return movieService.findAllByDisabled(true,search);
            case 2:
                return movieService.findAll(search);
        }
    }
}
