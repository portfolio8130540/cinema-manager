package com.example.cinemamanager.controller;


import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {


    @GetMapping
    @PreAuthorize("hasRole('LOGISTICIAN')")
    public String login( Authentication authentication){
        return authentication.getAuthorities().toString();
    }



}
