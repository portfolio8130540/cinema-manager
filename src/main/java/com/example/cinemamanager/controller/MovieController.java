package com.example.cinemamanager.controller;

import com.example.cinemamanager.entity.Movie;
import com.example.cinemamanager.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/movie")
@RequiredArgsConstructor
public class MovieController {

    public final MovieService movieService;

    @GetMapping
    public Movie getMovieById(@RequestParam Long id){
        if (id != null){
           return movieService.findById(id);
        }
        throw new RuntimeException();
    }
    @DeleteMapping
    public ResponseEntity<?> delete(@RequestParam long id){
        movieService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Movie movie){
        movieService.edit(movie);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Movie movie){
        movieService.save(movie);
        return ResponseEntity.ok().build();
    }
}
