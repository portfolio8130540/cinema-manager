package com.example.cinemamanager.controller;


import com.example.cinemamanager.entity.Movie;
import com.example.cinemamanager.mediator.WorkerZoneMediator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/workerzone")
@RequiredArgsConstructor
public class WorkerPanelController {

    public final WorkerZoneMediator workerZoneMediator;

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN','CHECKER','LOGISTICIAN')")
    public String index(){
        return "index";
    }

    @GetMapping("/movies")
    @PreAuthorize("hasAnyRole('ADMIN','LOGISTICIAN')")
    public String movies(Model model, @RequestParam(required = false) Integer access, @RequestParam(required = false) String search){
        access = access == null ? 0 : access;
        search = search == null ? (String) model.getAttribute("searchValue") : search;
        model.addAttribute("searchValue", search);
        model.addAttribute("movieForm",new Movie());
        model.addAttribute("access",access);
        model.addAttribute("movies",workerZoneMediator.findAllByDisabledAndSearch(access,search));
        return "movies";
    }
}
