package com.example.cinemamanager.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String uuid;
    private String title;
    private String director;
    private String movieCategory;
    private String ageCategory;
    private int movieLength;
    private String premiere;
    private String picture;
    private String trailer;
    private String description;
    private boolean deleted;
}
