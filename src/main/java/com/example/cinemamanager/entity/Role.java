package com.example.cinemamanager.entity;

public enum Role {
    ROLE_ADMIN,
    ROLE_CHECKER,
    ROLE_LOGISTICIAN

}
