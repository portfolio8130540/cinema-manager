package com.example.cinemamanager.service;

import com.example.cinemamanager.entity.Movie;
import com.example.cinemamanager.exceptions.MovieDontExistException;
import com.example.cinemamanager.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieService {

    private final MovieRepository movieRepository;

    public List<Movie> findAllByDisabled(boolean disabled, String search) {
        if (search == null || search.isBlank()) {
            return movieRepository.findMoviesByDeletedIsOrderByUuid(disabled);
        }
        return movieRepository.findMoviesByDeletedIsAndUuidOrAgeCategoryOrMovieCategoryOrTitleOrderByUuid(disabled, search, search, search, search);
    }

    public void save(Movie movie) {
        movie.setDeleted(false);
        movie = movieRepository.saveAndFlush(movie);
        movie.setUuid(movie.getId() + String.valueOf(movie.getMovieCategory().charAt(0)));
        movieRepository.save(movie);
    }

    public void edit(Movie movie) {
        movieRepository.findMoviesByDeletedIsAndUuid(false, movie.getUuid()).ifPresentOrElse(value -> {
            value.setDeleted(true);
            movieRepository.save(value);
            movieRepository.save(movie);
        }, MovieDontExistException::new);
    }

    public void delete(long id) {
        movieRepository.findById(id).ifPresentOrElse(value -> {
            value.setDeleted(true);
            movieRepository.save(value);
        }, MovieDontExistException::new);
    }

    public Movie findById(long id) {
        return movieRepository.findById(id).orElseThrow(MovieDontExistException::new);
    }

    public List<Movie> findAll(String search) {
        if (search == null || search.isBlank()){
            return movieRepository.findAll(Sort.by(Sort.Direction.ASC, "uuid"));
        }
        return movieRepository.findMoviesByUuidOrAgeCategoryOrMovieCategoryOrTitleOrderByUuid(search,search,search,search);

    }
}
